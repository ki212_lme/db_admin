CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'StrongPassword';

CREATE CERTIFICATE DBFootCert WITH SUBJECT ='Certificate for FootballDB'

USE [FootballDB]
CREATE DATABASE ENCRYPTION KEY
    WITH ALGORITHM = AES_128
    ENCRYPTION BY SERVER CERTIFICATE DBFootCert;

ALTER DATABASE [FootballDB]
    SET ENCRYPTION ON ;

SELECT DB_NAME(DB_ID('FootballDB')), * FROM sys.dm_database_encryption_keys
