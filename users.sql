use FootballDB

create login db_owner_admin with password = 'db_owner_admin'
create login sen_manager with password = 'sen_manager'
create login jun_manager with password = 'jun_manager'
create login observer with password = 'observer'

create user db_owner_admin for login db_owner_admin
create user sen_manager for login sen_manager
create user jun_manager for login jun_manager
create user observer for login observer

ALTER role db_owner add MEMBER db_owner_admin
exec sp_helprolemember

--sen manager
grant all on Championships to sen_manager
grant all on ChampionshipSeasons to sen_manager
grant all on ChampionshipSeasonTeams to sen_manager
grant all on ProvideToursTournamentGroup to sen_manager
grant select, UPDATE on Users to sen_manager
grant select, UPDATE, DELETE on RoleUser to sen_manager
grant select, UPDATE on Role to sen_manager
grant all on Countries to sen_manager
grant all on FinalStageTournaments to sen_manager
grant all on Players to sen_manager
grant all on Team to sen_manager
grant all on TournamentGroups to sen_manager
grant all on ChampionshipTeamsGroups to sen_manager
grant all on Matches to sen_manager
grant all on PlayerTeamSeason to sen_manager
grant all on [TeamsSheet<TournamentGroup>] to sen_manager
grant all on Tours to sen_manager
grant all on ProvideToursChampionshipSeason to sen_manager
grant all on TourMatchs to sen_manager


--jun manager
grant select on Championships to jun_manager
grant select, Update on ChampionshipSeasons to jun_manager
grant select, Update on ChampionshipSeasonTeams to jun_manager
grant all on ProvideToursTournamentGroup to jun_manager
grant select on Users to jun_manager
grant select on RoleUser to jun_manager
grant select on Role to jun_manager
grant select, Update on Countries to jun_manager
grant select, Update on FinalStageTournaments to jun_manager
grant select, Update on Players to jun_manager
grant select, Update on Team to jun_manager
grant select, Update on TournamentGroups to jun_manager
grant select, Update on ChampionshipTeamsGroups to jun_manager
grant select, Update on Matches to jun_manager
grant select, Update on PlayerTeamSeason to jun_manager
grant select, Update on [TeamsSheet<TournamentGroup>] to jun_manager
grant select, Update on Tours to jun_manager
grant select, Update on ProvideToursChampionshipSeason to jun_manager
grant select, Update on TourMatchs to jun_manager

-- observer
grant select on Championships to observer
grant select on ChampionshipSeasons to observer
grant select on ChampionshipSeasonTeams to observer
grant all on ProvideToursTournamentGroup to observer
Deny all on Users to observer
Deny all on RoleUser to observer
Deny all on Role to observer
grant select on Countries to observer
grant select on FinalStageTournaments to observer
grant select on Players to observer
grant select on Team to observer
grant select on TournamentGroups to observer
grant select on ChampionshipTeamsGroups to observer
grant select on Matches to observer
grant select on PlayerTeamSeason to observer
grant select on [TeamsSheet<TournamentGroup>] to observer
grant select on Tours to sen_manager
grant select on ProvideToursChampionshipSeason to observer
grant select on TourMatchs to observer

exec as user  = 'observer'
select * from Users
select *from Players