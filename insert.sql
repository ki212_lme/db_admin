use FootballDB
insert into Countries (NameCountry)
values ('Ukraine'),
       ('Italia'),
       ('England');
insert into Championships (Name, CountryId)
values ('Upl', 1),
       ('Seria A', 2),
       ('EPL', 3);
insert into ChampionshipSeasons (ChampionshipId, StartSeason, EndSeason)
values (1, '2023-12-12', '2024-12-12'),
       (2, '2023-12-12', '2024-12-12'),
       (3, '2023-12-12', '2024-12-12')
insert into Users (UserName, Email, PasswordHash)
values ('maks', 'lyamo.maks@gmail.com', 'qhruqhwieruqwebbiqigquier1238r12'),
       ('nikita', 'nikita@gmail.com', 'qrvqewrvqewrvqwervqewrvqewrvqwerv'),
       ('dima', 'dima@gmail.com', 'qwevrqwevrqwvrqwvrqwvrqwvr')
insert into Players (FirstName, SecondName, TransferPrice, Height, CountryId, DateBorn)
values ('max', 'limont', 1000000, 18, 1, '2003-12-26'),
       ('dima', 'grab', 1000000, 18, 2, '2002-12-26'),
       ('ni', 'dra', 1000000, 18, 3, '2004-11-26')
INSERT INTO Team (NameTeam, Place, CountryId)
VALUES ('TeamA', 'CityA', 1),
       ('TeamB', 'CityB', 2),
       ('TeamC', 'CityC', 3);
insert into ChampionshipSeasonTeams (TeamId, SeasonId)
values (1, 1),
       (2, 2),
       (2, 2)
insert into TournamentGroups (GroupName, TournamentId)
values ('A', 1),
       ('B', 1),
       ('C', 1)
insert into [TeamsSheet<TournamentGroup>] (teamId, ProviderId, Points)
values (1, 1, 12),
       (2, 1, 12),
       (3, 1, 12)
insert into ChampionshipTeamsGroups (teamId, ProviderId, Points)
values (1, 1, 12),
       (2, 1, 12),
       (3, 1, 12)
insert into Matches (HomeTeamId, GuestTeamId, Place, DateTimeMatchDay)
values (1, 2, 'Kyiv', '2023-12-12'),
       (1, 3, 'Ukraine, Zhytomyr', '2023-11-12'),
       (3, 2, 'Dnipro', '2023-10-12')
insert into PlayerTeamSeason (TeamId, ChampionshipSeasonId, PlayerId)
values (1, 1, 1),
       (2, 2, 2),
       (3, 3, 3)
insert into Tours (NumberTour)
values (1),
       (2),
       (3),
       (4),
       (5),
       (6)
insert into ProvideToursChampionshipSeason (TourId, ProviderId)
values (1, 1),
       (2, 1),
       (3, 2)

insert into ProvideToursTournamentGroup (TourId, ProviderId)
values (4, 1),
       (5, 1),
       (6, 2)

insert into TourMatchs (tourid, matchid)
values (1, 1),
       (2, 2),
       (3, 3)

insert into Role(nameRole, domain, permission)
values ('admin', 'users', 32),
       ('admin', 'championship', 2),
       ('admin', 'role', 4)

insert into RoleUser(IdUser, RoleId)
values (1,1),
       (2,2),
       (3,3)


use FootballDb
select * from ChampionshipSeasons