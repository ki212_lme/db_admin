create database FootballDB
use FootballDB
create table Users
(
    Id           int identity
        constraint PK_AspNetUsers
            primary key,
    UserName     nvarchar(256),
    Email        nvarchar(256),
    PasswordHash nvarchar(max),
)
go

create index IX_Users_Id
    on Users (Id)
go


create table Countries
(
    Id          int identity
        constraint PK_Countries
            primary key,

    NameCountry nvarchar(max)
)
go

create table Championships
(
    Id        int identity
        constraint PK_Championships
            primary key,
    Name      nvarchar(max),
    CountryId int not null
        constraint FK_Championships_Countries_CountryId
            references Countries
            on delete cascade
)
go

create table ChampionshipSeasons
(
    ChampionshipSeasonId int identity
        constraint PK_ChampionshipSeasons
            primary key,
    ChampionshipId       int       not null
        constraint FK_ChampionshipSeasons_Championships_ChampionshipId
            references Championships
            on delete cascade,
    StartSeason          datetime2 not null,
    EndSeason            datetime2 not null
)
go

create index IX_ChampionshipSeasons_ChampionshipId
    on ChampionshipSeasons (ChampionshipId)
go

create index IX_ChampionshipSeasons_ChampionshipSeasonId
    on ChampionshipSeasons (ChampionshipSeasonId)
go

create index IX_Championships_CountryId
    on Championships (CountryId)
go

create index IX_Championships_Id
    on Championships (Id)
go

create index IX_Countries_Id
    on Countries (Id)
go

create table FinalStageTournaments
(
    Id              int identity
        constraint PK_FinalStageTournaments
            primary key,
    TournamentStage int,
    MatchId         int,
    FOREIGN KEY (MatchId) REFERENCES Matches (Id) on delete no action,
    TournamentId    int not null
        constraint FK_FinalStageTournaments_ChampionshipSeasons_TournamentId
            references ChampionshipSeasons
            on delete cascade
)
go

create index IX_FinalStageTournaments_TournamentId
    on FinalStageTournaments (TournamentId)
go

create table Players
(
    Id            int identity
        constraint PK_Players
            primary key,
    FirstName     nvarchar(max) not null,
    SecondName    nvarchar(max) not null,
    TransferPrice money         not null,
    Height        int           not null,
    CountryId     int           not null
        constraint FK_Players_Countries_CountryId
            references Countries
            on delete cascade,
    DateBorn      datetime2     not null
)
go

create index IX_Players_CountryId
    on Players (CountryId)
go

create index IX_Players_Id
    on Players (Id)
go

create table Team
(
    Id        int identity
        constraint PK_Team
            primary key,
    NameTeam        nvarchar(max),
    Place     nvarchar(max),
    CountryId int not null
        constraint FK_Team_Countries_CountryId
            references Countries
)
go


create table ChampionshipSeasonTeams
(
    Id       int identity
        constraint PK_ChampionshipSeasonTeams
            primary key,
    TeamId   int not null
        constraint FK_ChampionshipSeasonTeams_Team_TeamId
            references Team,
    SeasonId int not null
        constraint FK_ChampionshipSeasonTeams_ChampionshipSeasons_SeasonId
            references ChampionshipSeasons
)
go

create index IX_ChampionshipSeasonTeams_Id
    on ChampionshipSeasonTeams (Id)
go

create index IX_ChampionshipSeasonTeams_SeasonId
    on ChampionshipSeasonTeams (SeasonId)
go

create index IX_ChampionshipSeasonTeams_TeamId
    on ChampionshipSeasonTeams (TeamId)
go

create table ChampionshipTeamsGroups
(
    Id         int identity
        constraint PK_ChampionshipTeamsGroups
            primary key,
    TeamId     int not null
        constraint FK_ChampionshipTeamsGroups_Team_ProviderId
            references Team
            on delete cascade,
    ProviderId int not null
        constraint FK_ChampionshipTeamsGroups_ChampionshipSeasons_ProviderId
            references ChampionshipSeasons
            on delete cascade,
    Points     int not null
)
go

create index IX_ChampionshipTeamsGroups_Id
    on ChampionshipTeamsGroups (Id)
go

create index IX_ChampionshipTeamsGroups_ProviderId
    on ChampionshipTeamsGroups (ProviderId)
go

create table Matches
(
    Id               int identity
        constraint PK_Matches
            primary key,
    HomeTeamId       int
        constraint FK_Matches_Team_HomeTeamId
            references Team,
    GuestTeamId      int
        constraint FK_Matches_Team_GuestTeamId
            references Team,
    Place            nvarchar(max),
    HomeGoals int,
    GuestGoal int,
    DateTimeMatchDay datetime2 not null
)
go

create index IX_Matches_GuestTeamId
    on Matches (GuestTeamId)
go

create index IX_Matches_HomeTeamId
    on Matches (HomeTeamId)
go

create index IX_Matches_Id
    on Matches (Id)
go

create table PlayerTeamSeason
(
    Id                   int identity
        constraint PK_PlayerTeamSeason
            primary key,
    TeamId               int
        constraint FK_PlayerTeamSeason_Team_TeamId
            references Team,
    ChampionshipSeasonId int
        constraint FK_PlayerTeamSeason_ChampionshipSeasons_ChampionshipSeasonId
            references ChampionshipSeasons,
    PlayerId             int
        constraint FK_PlayerTeamSeason_Players_PlayerId
            references Players
)
go

create index IX_PlayerTeamSeason_ChampionshipSeasonId
    on PlayerTeamSeason (ChampionshipSeasonId)
go

create index IX_PlayerTeamSeason_Id
    on PlayerTeamSeason (Id)
go

create index IX_PlayerTeamSeason_PlayerId
    on PlayerTeamSeason (PlayerId)
go

create index IX_PlayerTeamSeason_TeamId
    on PlayerTeamSeason (TeamId)
go

create index IX_Team_CountryId
    on Team (CountryId)
go

create index IX_Team_Id
    on Team (Id)
go

create table TournamentGroups
(
    GroupId      int identity
        constraint PK_TournamentGroups
            primary key,
    GroupName    nvarchar(max) not null,
    TournamentId int           not null
        constraint FK_TournamentGroups_ChampionshipSeasons_TournamentId
            references ChampionshipSeasons
            on delete cascade
)
go

create table [TeamsSheet<TournamentGroup>]
(
    Id         int identity
        constraint [PK_TeamsSheet<TournamentGroup>]
            primary key,
    TeamId     int not null
        constraint [FK_TeamsSheet<TournamentGroup>_Team_ProviderId]
            references Team
            on delete cascade,
    ProviderId int not null
        constraint [FK_TeamsSheet<TournamentGroup>_TournamentGroups_ProviderId]
            references TournamentGroups
            on delete cascade,
    Points     int not null
)
go

create index [IX_TeamsSheet<TournamentGroup>_Id]
    on [TeamsSheet<TournamentGroup>] (Id)
go

create index [IX_TeamsSheet<TournamentGroup>_ProviderId]
    on [TeamsSheet<TournamentGroup>] (ProviderId)
go

create index IX_TournamentGroups_GroupId
    on TournamentGroups (GroupId)
go

create index IX_TournamentGroups_TournamentId
    on TournamentGroups (TournamentId)
go

create table Tours
(
    Id         int identity
        constraint PK_Tours
            primary key,
    NumberTour int not null
)
go

create table ProvideToursChampionshipSeason
(
    Id         int identity
        constraint PK_ProvideToursChampionshipSeason
            primary key,
    TourId     int not null
        constraint FK_ProvideToursChampionshipSeason_Tours_TourId
            references Tours
            on delete cascade,
    ProviderId int not null
        constraint FK_ProvideToursChampionshipSeason_ChampionshipSeasons_ProviderId
            references ChampionshipSeasons
            on delete cascade
)
go

create index IX_ProvideToursChampionshipSeason_Id
    on ProvideToursChampionshipSeason (Id)
go

create index IX_ProvideToursChampionshipSeason_ProviderId
    on ProvideToursChampionshipSeason (ProviderId)
go

create index IX_ProvideToursChampionshipSeason_TourId
    on ProvideToursChampionshipSeason (TourId)
go

create table ProvideToursTournamentGroup
(
    Id         int identity
        constraint PK_ProvideToursTournamentGroup
            primary key,
    TourId     int not null
        constraint FK_ProvideToursTournamentGroup_Tours_TourId
            references Tours
            on delete cascade,
    ProviderId int not null
        constraint FK_ProvideToursTournamentGroup_TournamentGroups_ProviderId
            references TournamentGroups
            on delete cascade
)
go

create index IX_ProvideToursTournamentGroup_Id
    on ProvideToursTournamentGroup (Id)
go

create index IX_ProvideToursTournamentGroup_ProviderId
    on ProvideToursTournamentGroup (ProviderId)
go

create index IX_ProvideToursTournamentGroup_TourId
    on ProvideToursTournamentGroup (TourId)
go

create table TourMatchs
(
    Id      int identity
        constraint PK_TourMatchs
            primary key,
    TourId  int
        constraint FK_TourMatchs_Tours_TourId
            references Tours,
    MatchId int
        constraint FK_TourMatchs_Matches_MatchId
            references Matches
)
go

create index IX_TourMatchs_Id
    on TourMatchs (Id)
go

create index IX_TourMatchs_MatchId
    on TourMatchs (MatchId)
go

create index IX_TourMatchs_TourId
    on TourMatchs (TourId)
go


create table RoleUser
(
    Id     int identity
        constraint PK_RoleUser
            primary key,
    IdUser int not null,
    FOREIGN KEY (IdUser) REFERENCES Users (Id) on delete cascade,
    RoleId int not null,
    FOREIGN KEY (RoleId) REFERENCES Role (Id) on delete cascade
)

create table Role
(
    Id         int identity
        constraint PK_Role
            primary key,
    NameRole   nvarchar(max),
    Domain     nvarchar(max),
    Permission int
)